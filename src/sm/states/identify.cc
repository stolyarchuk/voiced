#include "identify.h"

#include "app/person.h"
#include "utils/logger.h"
namespace alfa {

void Identify::Start() {
  const aricpp::JsonTree& ev = GetEv();
  const std::string& caller_num = aricpp::Get<std::string>(ev, {"channel", "caller", "number"});

  auto pqxx_conn = GetConn<DBNew>();
  pqxx::result pqxx_result = pqxx_conn.Select("get_person_by_phone", caller_num);

  person_ = Person::FromPqxx(pqxx_result);

  if (person_) {
    pqxx_result = pqxx_conn.Select("get_person_importance", person_->id);
    importance_ = Importance::FromPqxx(pqxx_result);
  }

  if (person_)
    LOG(INFO) << *person_;
  if (importance_)
    LOG(INFO) << *importance_;
}

Person::Ptr Identify::GetPerson() const { return person_; }
Importance::Ptr Identify::GetImportance() const { return importance_; }

}  // namespace alfa
