#ifndef ALFA_OPTIONS_H
#define ALFA_OPTIONS_H

#include <map>
#include <string>
#include <variant>

#include "CLI/CLI.hpp"

#include "voiced_version.h"

namespace alfa {

class Options {
 public:
  using Variant = std::variant<size_t, std::string>;

  static Options& Instance() {
    static Options s;
    return s;
  }

  Options(Options const&) = delete;
  Options& operator=(Options const&) = delete;

  void Init(int argc, char** argv);

  class ProxyValue {
   public:
    ProxyValue(Options* opts, const std::string& k) : opts_{opts}, k_{k} {}

    template <typename T>
    operator T() {
      return opts_->Get<T>(k_);
    }

   private:
    Options* opts_;
    std::string k_;
  };

  ProxyValue operator[](const std::string& key) { return ProxyValue(this, key); }

  template <typename T>
  inline T Get(const std::string& key) const {
    return std::get<T>(data_.at(key));
  }

 private:
  Options() = default;
  CLI::App opts_{PROJECT_NAME};

  std::map<std::string, Variant> data_{
      {"threads", 4},           {"ari-host", "127.0.0.1"},    {"ari-port", "8088"},
      {"ari-user", "asterisk"}, {"ari-pass", "asterisk"},     {"ari-apps", "person_priority"},
      {"pool-size", 4},         {"pool-max-size", 48},        {"db-new-host", "127.0.0.1"},
      {"db-new-port", "5432"},  {"db-new-user", "alfa_user"}, {"db-new-pass", "alfa_pass"},
      {"db-new-name", "alfa"},  {"db-old-host", "127.0.0.1"}, {"db-old-port", "5433"},
      {"db-old-user", "ansec"}, {"db-old-pass", "ansec"},     {"db-old-name", "ansec"}};
};

}  // namespace alfa

#endif  // ALFA_OPTIONS_H
