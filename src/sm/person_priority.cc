#include "person_priority.h"

#include "utils/logger.h"

namespace alfa {

void PersonPriority::Start() {
  sm_.process_event(Answer{});
  sm_.process_event(ContinueInDialplan{});
}

}  // namespace alfa
