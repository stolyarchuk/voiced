#ifndef ALFA_CONTINUE_H
#define ALFA_CONTINUE_H

#include "app/importance.h"
#include "app/person.h"

#include "base_state.h"
namespace alfa {

class Continue : public IState {
 public:
  Continue() = default;
  ~Continue() override = default;

  void Start() override;
  void SetPerson(Person::Ptr person);
  void SetImportance(Importance::Ptr importance);

 private:
  Person::Ptr person_{nullptr};
  Importance::Ptr importance_{nullptr};
};

}  // namespace alfa

#endif  // ALFA_CONTINUE_H
