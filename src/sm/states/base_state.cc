#include "base_state.h"

namespace alfa {

void IState::SetEv(aricpp::JsonTree& ev) { ev_ = ev; }
void IState::SetClient(aricpp::Client::Ptr client) { client_ = client; }
void IState::SetChannel(aricpp::Channel::Ptr channel) { channel_ = channel; }

aricpp::JsonTree& IState::GetEv() { return ev_; }
aricpp::Client::Ptr IState::GetClient() const { return client_; }
aricpp::Channel::Ptr IState::GetChannel() const { return channel_; }

}  // namespace alfa
