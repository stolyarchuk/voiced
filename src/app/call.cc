#include "call.h"

#include "fmt/format.h"
#include "utils/logger.h"

namespace alfa {

Call::Call(boost::asio::io_context& io, aricpp::Client::Ptr client, const aricpp::JsonTree& ev)
    : io_{io}
    , ev_{ev}
    , client_{client} {
  channel_id_ = aricpp::Get<std::string>(ev_, {"channel", "id"});
  caller_num_ = aricpp::Get<std::string>(ev_, {"channel", "caller", "number"});
  stasis_app_ = aricpp::Get<std::string>(ev_, {"application"});
  channel_ = std::make_shared<aricpp::Channel>(*client_, channel_id_);
}

void Call::Start() {
  switch (_hash(stasis_app_.c_str())) {
    case "person_priority"_:
    case "PersonPriority"_:
      sm_ = std::make_shared<PersonPriority>(pool_new_, pool_old_, client_, channel_, ev_);
      break;
    case "object_control_"_:
    case "ObjectControl"_:
      sm_ = std::make_shared<PersonPriority>(pool_new_, pool_old_, client_, channel_, ev_);
      break;
    default:
      LOG(ERROR) << "unknown stasis app: " << stasis_app_;
      sm_ = nullptr;
  }

  if (nullptr != sm_)
    io_.post([&] { sm_->Start(); });
}

}  // namespace alfa
