#include "options.h"

#include "utils/logger.h"

namespace alfa {

void Options::Init(int argc, char** argv) {
  opts_.add_option<Variant, size_t>("--pool-size", data_["pool-size"], "DB pool initial size");
  opts_.add_option<Variant, size_t>("--pool-max-size", data_["pool-max-size"], "DB pool max size");
  opts_.add_option<Variant, size_t>("--threads", data_["threads"], "Number of IO threads");
  opts_.add_option<Variant, std::string>("--ari-host", data_["ari-host"], "ARI host");
  opts_.add_option<Variant, std::string>("--ari-port", data_["ari-port"], "ARI port");
  opts_.add_option<Variant, std::string>("--ari-user", data_["ari-user"], "ARI user");
  opts_.add_option<Variant, std::string>("--ari-pass", data_["ari-pass"], "ARI password");
  opts_.add_option<Variant, std::string>("--ari-apps", data_["ari-apps"], "ARI applications");
  opts_.add_option<Variant, std::string>("--db-new-host", data_["db-new-host"], "New DB host");
  opts_.add_option<Variant, std::string>("--db-new-port", data_["db-new-port"], "New DB port");
  opts_.add_option<Variant, std::string>("--db-new-user", data_["db-new-user"], "New DB username");
  opts_.add_option<Variant, std::string>("--db-new-pass", data_["db-new-pass"], "New DB password");
  opts_.add_option<Variant, std::string>("--db-new-name", data_["db-new-name"], "New DB name");
  opts_.add_option<Variant, std::string>("--db-old-host", data_["db-old-host"], "Old DB host");
  opts_.add_option<Variant, std::string>("--db-old-port", data_["db-old-port"], "Old DB port");
  opts_.add_option<Variant, std::string>("--db-old-user", data_["db-old-user"], "Old DB username");
  opts_.add_option<Variant, std::string>("--db-old-pass", data_["db-old-pass"], "Old DB password");
  opts_.add_option<Variant, std::string>("--db-old-name", data_["db-old-name"], "Old DB name");

  SeverityLevel log_level;
  opts_.add_flag_function(
      "-v, --verbose",
      [&](int count) {
        log_level = SeverityLevel::DEBUG;

        if (count > 0 && count < 3)
          log_level = static_cast<SeverityLevel>(2 - count);
      },
      "Verbose level");

  try {
    opts_.parse(argc, argv);
    SET_LOG_LEVEL(log_level);
  } catch (const CLI::ParseError& e) {
    opts_.exit(e);
    throw e;
  }
}

}  // namespace alfa
