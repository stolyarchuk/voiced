#ifndef ALFA_PERSON_H
#define ALFA_PERSON_H

#include <memory>
#include <pqxx/pqxx>

namespace alfa {

struct Person {
  using Ptr = std::shared_ptr<Person>;

  Person() = default;

  static Person::Ptr FromPqxx(const pqxx::result& pqxx);

  inline friend std::ostream& operator<<(std::ostream& os, const Person& p) {
    return os << "<Person id: " << p.id << "; " << p.first_name << ' ' << p.second_name << ">";
  }

  std::string GetFullName() const;

  int64_t id{-1};
  std::string first_name;
  std::string second_name;
};

}  // namespace alfa

#endif  // ALFA_PERSON_H
