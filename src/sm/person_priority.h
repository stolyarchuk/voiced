#ifndef ALFA_PERSONPRIORITY_H
#define ALFA_PERSONPRIORITY_H

#include <variant>

#include "app/db.h"
#include "app/importance.h"
#include "app/person.h"
#include "aricpp/channel.h"
#include "aricpp/client.h"
#include "fmt/format.h"
#include "states/continue.h"
#include "states/identify.h"

#include "base_state_machine.h"
#include "sml.hpp"

namespace sml = boost::sml;
namespace alfa {

struct SMLogger {
  template <class SM, class TEvent>
  void log_process_event(const TEvent&) {
    LOG(DEBUG) << fmt::format("[{}][process_event] {}", sml::aux::get_type_name<SM>(),
                              sml::aux::get_type_name<TEvent>());
  }

  template <class SM, class TGuard, class TEvent>
  void log_guard(const TGuard&, const TEvent&, bool result) {
    LOG(DEBUG) << fmt::format("[{}][guard] {} {} {}", sml::aux::get_type_name<SM>(),
                              sml::aux::get_type_name<TGuard>(), sml::aux::get_type_name<TEvent>(),
                              (result ? "[OK]" : "[Reject]"));
  }

  template <class SM, class TAction, class TEvent>
  void log_action(const TAction&, const TEvent&) {
    LOG(DEBUG) << fmt::format("[{}][action] {} {}", sml::aux::get_type_name<SM>(),
                              sml::aux::get_type_name<TAction>(), sml::aux::get_type_name<TEvent>());
  }

  template <class SM, class TSrcState, class TDstState>
  void log_state_change(const TSrcState& src, const TDstState& dst) {
    LOG(DEBUG) << fmt::format("[{}][transition] {} -> {}", sml::aux::get_type_name<SM>(), src.c_str(),
                              dst.c_str());
  }
};

struct PersonPrioritySM {
  template <typename New, typename Old, typename Client, typename Channel, typename JsonTree>
  explicit PersonPrioritySM(New pool_new, Old pool_old, Client client, Channel channel, JsonTree& ev)
      : pool_new_{pool_new}
      , pool_old_{pool_old}
      , client_{client}
      , channel_{channel}
      , ev_{ev} {}

  inline auto operator()() {
    using namespace boost::sml;
    // clang-format off
      return make_transition_table(
        *"idle"_s + event<Answer> / [this] {
        state_ = Identify{};
        std::get<Identify>(state_).SetPools(pool_new_,pool_old_);
        std::get<Identify>(state_).SetEv(ev_);
        std::get<Identify>(state_).Start();
        person_ = std::get<Identify>(state_).GetPerson();
        importance_ = std::get<Identify>(state_).GetImportance();
      } = state<Identify>
      , state<Identify> + event<ContinueInDialplan> / [this] {
        state_ = Continue{};
        std::get<Continue>(state_).SetChannel(channel_);
        std::get<Continue>(state_).SetPerson(person_);
        std::get<Continue>(state_).SetImportance(importance_);
        std::get<Continue>(state_).Start();
      } = state<Continue>
        , state<Continue> = X
      );
    // clang-format on
  }

 private:
  DBPool<DBNew>::Ptr pool_new_;
  DBPool<DBOld>::Ptr pool_old_;
  std::variant<Identify, Continue> state_{};
  aricpp::Client::Ptr client_{nullptr};
  aricpp::Channel::Ptr channel_{nullptr};
  aricpp::JsonTree& ev_;
  Person::Ptr person_;
  Importance::Ptr importance_;
};

class PersonPriority : public IStateMachine {
 public:
  template <typename New, typename Old, typename Client, typename Channel, typename JsonTree>
  PersonPriority(New pool_new, Old pool_old, Client client, Channel channel, JsonTree& ev)
      : logger_{}
      , sm_{logger_, PersonPrioritySM{pool_new, pool_old, client, channel, ev}} {}
  ~PersonPriority() override = default;

  void Start() override;

 private:
  SMLogger logger_;
  sml::sm<PersonPrioritySM, sml::logger<SMLogger>> sm_;

  Person::Ptr person_{nullptr};
  Importance::Ptr importance_{nullptr};
};

}  // namespace alfa

#endif  // ALFA_PERSONPRIORITY_H
