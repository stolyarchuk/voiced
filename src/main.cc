#include <iostream>

#include "app/voiced.h"

int main(int argc, char* argv[]) {
  int exit_code = EXIT_SUCCESS;

  try {
    alfa::Voiced app(argc, argv);
    app.Start();
  } catch (const CLI::ParseError&) {
    exit_code = EXIT_FAILURE;
  } catch (const std::exception& e) {
    LOG(ERROR) << e.what();
    exit_code = EXIT_FAILURE;
  }

  return exit_code;
}
