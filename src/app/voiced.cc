#include "voiced.h"

#include <thread>

#include <boost/algorithm/string.hpp>

#include "aricpp/channel.h"
#include "aricpp/jsontree.h"

namespace alfa {

Voiced::Voiced(int argc, char** argv) : io_{}, signals_{io_} {
  Options::Instance().Init(argc, argv);

  signals_.add(SIGINT);
  signals_.add(SIGTERM);
  signals_.add(SIGQUIT);
  signals_.add(SIGHUP);

  signals_.async_wait([&](boost::system::error_code const& ec, int sig) {
    if (ec)
      throw std::runtime_error(ec.message());

    LOG(INFO) << "received signal: " << sig;

    if (sig != SIGHUP)
      Stop();
  });
}

Voiced::~Voiced() {
  for (auto&& io_thread : io_threads_)
    io_thread->join();
}

void Voiced::Start() {
  auto& opts = Options::Instance();

  const std::string& new_conn_str = IDataBase::CreateConnString(
      opts["db-new-host"], opts["db-new-port"], opts["db-new-user"], opts["db-new-pass"], opts["db-new-name"]);

  const std::string& old_conn_str = IDataBase::CreateConnString(
      opts["db-old-host"], opts["db-old-port"], opts["db-old-user"], opts["db-old-pass"], opts["db-old-name"]);

  pool_new_ = std::make_shared<DBPool<DBNew>>(io_, new_conn_str);
  pool_old_ = std::make_shared<DBPool<DBOld>>(io_, old_conn_str);

  ari_.FromOpts(opts);

  client_ = std::make_shared<aricpp::Client>(io_, ari_.host, ari_.port, ari_.user, ari_.pass, ari_.apps);
  client_->Connect([&](const boost::system::error_code& ec) {
    if (ec)
      throw std::runtime_error(fmt::format("{} by {}", ec.message(), ari_.Dump()));

    SetupEvents();
  });

  size_t total_threads = opts["threads"];

  for (size_t i = 0; i < total_threads; ++i)
    io_threads_.emplace_back(std::make_unique<std::thread>([&] { io_.run(); }));

  LOG(MAIN) << PROJECT_NAME << " started (version: " << PROJECT_VER << "; build: " << PROJECT_BUILD_DATE << ')';
}

void Voiced::Stop() {
  if (!io_.stopped())
    io_.stop();

  LOG(MAIN) << PROJECT_NAME << " stopped (version: " << PROJECT_VER << "; build: " << PROJECT_BUILD_DATE << ')';
}

void Voiced::SetupEvents() {
  client_->OnEvent("StasisStart", [&](const aricpp::JsonTree& ev) {
    try {
      auto call = std::make_shared<Call>(io_, client_, ev);
      auto channel_id = call->GetChannelId();

      call->SetPools(pool_new_, pool_old_);
      call->Start();
      calls_[channel_id] = call;

      LOG(DEBUG) << "call started: " << aricpp::ToString(ev);
    } catch (std::exception& e) {
      LOG(ERROR) << aricpp::ToString(ev) << ": " << e.what();
    }
  });

  client_->OnEvent("StasisEnd", [&](const aricpp::JsonTree& ev) {
    try {
      auto channel_id = aricpp::Get<std::string>(ev, {"channel", "id"});

      if (calls_.count(channel_id))
        calls_.erase(channel_id);

    } catch (std::exception& e) {
      LOG(ERROR) << aricpp::ToString(ev) << ": " << e.what();
    }
  });

  client_->OnEvent("ChannelDtmfReceived", [&](const aricpp::JsonTree& ev) {
    try {
      //      LOG(DEBUG) << aricpp::ToString(ev);
    } catch (std::exception& e) {
      LOG(ERROR) << aricpp::ToString(ev) << ": " << e.what();
    }
  });

  client_->OnEvent("ChannelHangupRequest", [&](const aricpp::JsonTree& ev) {
    try {
      LOG(DEBUG) << "call ended: " << aricpp::ToString(ev);
    } catch (std::exception& e) {
      LOG(ERROR) << aricpp::ToString(ev) << ": " << e.what();
    }
  });

  LOG(INFO) << "connected to asterisk ARI: " << ari_.host << ':' << ari_.port;
}

}  // namespace alfa
