#include "person.h"

#include "fmt/format.h"

namespace alfa {

Person::Ptr Person::FromPqxx(const pqxx::result& pqxx) {
  if (pqxx.empty())
    return nullptr;

  Person::Ptr person = std::make_shared<Person>();

  const auto& row = pqxx.at(0);

  if (!row.at(0).is_null())
    person->id = row.at(0).as<int64_t>();

  if (!row.at(1).is_null())
    person->first_name = row.at(1).as<std::string>();

  if (!row.at(2).is_null())
    person->second_name = row.at(2).as<std::string>();

  return person;
}

std::string Person::GetFullName() const {
  std::string full_name{};

  full_name = first_name;

  if (!second_name.empty())
    full_name = fmt::format("{} {}", full_name, second_name);

  return full_name;
}

}  // namespace alfa
