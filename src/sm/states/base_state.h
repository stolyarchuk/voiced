#ifndef ALFA_ISTATE_H
#define ALFA_ISTATE_H

#include <type_traits>

#include "app/db.h"
#include "aricpp/channel.h"
#include "aricpp/client.h"

namespace alfa {

struct Answer {};
struct ContinueInDialplan {};

class IState {
 public:
  IState() = default;
  virtual ~IState() = default;
  template <typename New, typename Old>
  void SetPools(New pool_new, Old pool_old) {
    pool_new_ = pool_new;
    pool_old_ = pool_old;
  }

  virtual void Start() = 0;
  void SetEv(aricpp::JsonTree& ev);
  void SetClient(aricpp::Client::Ptr);
  void SetChannel(aricpp::Channel::Ptr);
  aricpp::JsonTree& GetEv();
  aricpp::Client::Ptr GetClient() const;
  aricpp::Channel::Ptr GetChannel() const;

 protected:
  template <typename P>
  auto GetConn() const {
    if constexpr (std::is_same<P, DBNew>::value)
      return DBConn<DBNew>(pool_new_);
    else
      return DBConn<DBOld>(pool_old_);
  }

 private:
  DBPool<DBNew>::Ptr pool_new_;
  DBPool<DBOld>::Ptr pool_old_;

  aricpp::JsonTree ev_;
  aricpp::Client::Ptr client_;
  aricpp::Channel::Ptr channel_;
};

}  // namespace alfa

#endif  // ALFA_ISTATE_H
