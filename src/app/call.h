#ifndef ALFA_CALL_H
#define ALFA_CALL_H

#include <memory>

#include <boost/asio.hpp>

#include "aricpp/channel.h"
#include "aricpp/client.h"
#include "sm/person_priority.h"

#include "db.h"

constexpr unsigned int _hash(const char* str, int h = 0) {
  return !str[h] ? 5381 : (_hash(str, h + 1) * 33) ^ static_cast<unsigned int>(str[h]);
}

constexpr auto operator"" _(char const* p, size_t) { return _hash(p); }

namespace alfa {

class Call {
 public:
  using Ptr = std::shared_ptr<Call>;

  Call(boost::asio::io_context& io_, aricpp::Client::Ptr client, const aricpp::JsonTree& ev);
  std::string GetChannelId() const { return channel_id_; }

  template <typename NewPool, typename OldPool>
  void SetPools(NewPool pool_new, OldPool pool_old) {
    pool_new_ = pool_new;
    pool_old_ = pool_old;
  }

  template <typename SM>
  void SetStateMachine(SM sm) {
    sm_ = sm;
  }

  void Start();

 private:
  boost::asio::io_context& io_;

  aricpp::JsonTree ev_;
  aricpp::Client::Ptr client_;
  aricpp::Channel::Ptr channel_;

  DBPool<DBNew>::Ptr pool_new_;
  DBPool<DBOld>::Ptr pool_old_;

  std::string stasis_app_;
  std::string channel_id_;
  std::string caller_num_;

  IStateMachine::Ptr sm_;
};

}  // namespace alfa

#endif  // ALFA_CALL_H
