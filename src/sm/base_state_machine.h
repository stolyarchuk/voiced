#ifndef ALFA_ISTATEMACHINE_H
#define ALFA_ISTATEMACHINE_H

#include <memory>

#include "aricpp/channel.h"
#include "aricpp/client.h"

namespace alfa {

class IStateMachine {
 public:
  using Ptr = std::shared_ptr<IStateMachine>;

  IStateMachine() = default;
  virtual ~IStateMachine() = default;
  virtual void Start() = 0;
};

}  // namespace alfa

#endif  // ALFA_ISTATEMACHINE_H
