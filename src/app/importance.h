#ifndef ALFA_IMPORTANCE_H
#define ALFA_IMPORTANCE_H

#include <memory>
#include <pqxx/pqxx>

namespace alfa {

struct Importance {
  using Ptr = std::shared_ptr<Importance>;
  Importance() = default;

  static Importance::Ptr FromPqxx(const pqxx::result& pqxx);

  inline friend std::ostream& operator<<(std::ostream& os, const Importance& i) {
    return os << "<Importance id: " << i.id << ";  prio: " << i.priority << "; " << i.title << ">";
  }

  int64_t id{-1};
  std::string title;
  uint64_t priority;
};

}  // namespace alfa

#endif  // ALFA_IMPORTANCE_H
