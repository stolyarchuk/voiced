#ifndef ALFA_IDENTIFY_H
#define ALFA_IDENTIFY_H

#include "app/db.h"
#include "app/importance.h"
#include "app/person.h"

#include "base_state.h"

namespace alfa {

class Identify : public IState {
 public:
  Identify() = default;
  ~Identify() override = default;

  void Start() override;

  Person::Ptr GetPerson() const;
  Importance::Ptr GetImportance() const;

 private:
  Person::Ptr person_{nullptr};
  Importance::Ptr importance_{nullptr};
};

}  // namespace alfa

#endif  // ALFA_IDENTIFY_H
