#ifndef ALFA_DB_H
#define ALFA_DB_H

#include <functional>
#include <mutex>
#include <pqxx/pqxx>
#include <stack>

#include <boost/asio.hpp>

#include "utils/logger.h"
#include "utils/options.h"

namespace alfa {

class IDataBase {
 public:
  IDataBase(boost::asio::io_context&, const std::string&, std::function<void()>&&);
  virtual ~IDataBase() = default;

  virtual void Prepare() = 0;
  static std::string CreateConnString(const std::string&, const std::string&, const std::string&,
                                      const std::string&, const std::string&);

  template <typename... Args>
  pqxx::result Select(const std::string& stmt, Args... args) {
    if (nullptr != pqxx_) {
      pqxx::nontransaction work(*pqxx_);
      return work.exec_prepared(stmt, std::forward<Args>(args)...);
    } else
      return pqxx::result{};
  }

  friend std::ostream& operator<<(std::ostream& os, const IDataBase& DB);

  void Connect();
  void Reconnect();

 protected:
  void PrepareStmt(const std::string&, const std::string&);
  std::string Esc(const std::string& sql) const;

 private:
  std::string conn_string_;
  std::unique_ptr<pqxx::connection> pqxx_;
  boost::asio::deadline_timer reconnect_timer_;
  std::function<void()> on_connected_;

  void Reset();
};

class DBNew : public IDataBase {
 public:
  using Ptr = std::shared_ptr<DBNew>;

  DBNew(boost::asio::io_context&, const std::string&);
  ~DBNew() override = default;

  void Prepare() override;
};

class DBOld : public IDataBase {
 public:
  using Ptr = std::shared_ptr<DBOld>;

  DBOld(boost::asio::io_context&, const std::string&);
  ~DBOld() override = default;

  void Prepare() override;
};

template <typename T>
class DBPool {
 public:
  using Ptr = std::shared_ptr<DBPool<T>>;

  DBPool(boost::asio::io_context& io, const std::string& conn_string)
      : io_{io}
      , cleanup_timer_{io}
      , conn_string_{conn_string} {
    auto& opts = Options::Instance();
    size_t pool_size = opts["pool-size"];

    for (size_t i = 0; i < pool_size; ++i) {
      auto conn = CreateConnection();
      Push(conn);
    }

    RechargeTimer();
  }

  auto Acqure() {
    std::lock_guard<std::mutex> locker(pool_mtx_);
    return pool_.empty() ? CreateConnection() : GetTopConnection();
  }

  void Push(std::shared_ptr<T> conn) {
    std::lock_guard<std::mutex> locker(pool_mtx_);
    pool_.push(conn);
  }

 private:
  boost::asio::io_context& io_;
  boost::asio::deadline_timer cleanup_timer_;
  std::string conn_string_;
  std::stack<std::shared_ptr<T>> pool_;
  std::mutex pool_mtx_;

  void RechargeTimer() {
    cleanup_timer_.expires_from_now(boost::posix_time::seconds(10));
    cleanup_timer_.async_wait(std::bind(&DBPool<T>::Cleanup, this));
  }

  void Cleanup() {
    auto& opts = Options::Instance();
    size_t pool_max_size = opts["pool-max-size"];

    auto do_cleanup = [&] {
      size_t diff = pool_.size() - pool_max_size;

      for (size_t i = 0; i < diff; ++i)
        GetTopConnection();
    };

    if (pool_.size() > pool_max_size)
      do_cleanup();

    RechargeTimer();
  }

  auto CreateConnection() {
    auto conn = std::make_shared<T>(io_, conn_string_);
    conn->Connect();
    return conn;
  }
  auto GetTopConnection() {
    auto conn = pool_.top();
    pool_.pop();
    return conn;
  }
};

template <typename P>
class DBConn {
 public:
  using DBPoolT = typename DBPool<P>::Ptr;

  DBConn(DBPoolT pool) {
    pool_ = pool;
    conn_ = pool_->Acqure();
  }
  ~DBConn() { pool_->Push(conn_); }

  template <typename... Args>
  pqxx::result Select(const std::string& stmt, Args... args) {
    try {
      return conn_->Select(stmt, std::forward<Args>(args)...);
    } catch (const std::exception& e) {
      LOG(ERROR) << *conn_ << e.what();
      conn_->Reconnect();
    }

    return pqxx::result{};
  }

 private:
  typename DBPool<P>::Ptr pool_;
  typename P::Ptr conn_;
};  // namespace alfa

}  // namespace alfa

#endif  // ALFA_DB_H
