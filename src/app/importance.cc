#include "importance.h"

namespace alfa {

Importance::Ptr Importance::FromPqxx(const pqxx::result& pqxx) {
  if (pqxx.empty())
    return nullptr;

  Importance::Ptr importance = std::make_shared<Importance>();

  const auto& row = pqxx.at(0);

  if (!row.at(0).is_null())
    importance->id = row.at(0).as<int64_t>();

  if (!row.at(1).is_null())
    importance->priority = row.at(1).as<uint64_t>();

  if (!row.at(2).is_null())
    importance->title = row.at(2).as<std::string>();

  return importance;
}

}  // namespace alfa
