#include "continue.h"

#include "fmt/format.h"
namespace alfa {

void Continue::Start() {
  auto channel = GetChannel();
  std::string fullname{};

  if (person_)
    fullname = person_->GetFullName();

  if (importance_) {
    fullname = fmt::format("{} ({})", fullname, importance_->title);
    channel->SetVar("QUEUE_PRIO", std::to_string(importance_->priority));
  }

  if (!fullname.empty())
    channel->SetVar("CALLERID(name)", fullname);

  channel->ContinueInDialplan();
}

void Continue::SetPerson(Person::Ptr person) { person_ = person; }
void Continue::SetImportance(Importance::Ptr importance) { importance_ = importance; }

}  // namespace alfa
