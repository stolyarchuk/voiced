#include "db.h"

#include "utils/logger.h"

namespace alfa {

IDataBase::IDataBase(boost::asio::io_context& io, const std::string& conn_string,
                     std::function<void()>&& on_connected)
    : conn_string_{conn_string}
    , reconnect_timer_{io}
    , on_connected_{std::move(on_connected)} {}

std::string IDataBase::CreateConnString(const std::string& host, const std::string& port, const std::string& user,
                                        const std::string& password, const std::string& dbname) {
  {
    std::ostringstream db_string_ss;
    db_string_ss << "host=" << host           //
                 << " port=" << port          //
                 << " user=" << user          //
                 << " password=" << password  //
                 << " dbname=" << dbname;
    return db_string_ss.str();
  }
}

std::ostream& operator<<(std::ostream& os, const IDataBase& DB) {
  return os << '[' << (DB.pqxx_ ? DB.pqxx_->dbname() : "") << "]: ";
}

void IDataBase::PrepareStmt(const std::string& name, const std::string& definition) {
  if (pqxx_)
    pqxx_->prepare(name, definition);
}

std::string IDataBase::Esc(const std::string& sql) const { return pqxx_->esc(sql); }

void IDataBase::Connect() {
  try {
    pqxx_ = std::make_unique<pqxx::connection>(conn_string_);
    on_connected_();
    LOG(INFO) << "connected to db: " << pqxx_->dbname();

  } catch (const pqxx::broken_connection& e) {
    LOG(ERROR) << "connect error:" << e.what();
    Reconnect();
  }
}

void IDataBase::Reconnect() {
  Reset();

  reconnect_timer_.expires_from_now(boost::posix_time::seconds(5));
  reconnect_timer_.async_wait(std::bind(&IDataBase::Connect, this));
}

void IDataBase::Reset() {
  if (pqxx_)
    pqxx_.release();
}

DBNew::DBNew(boost::asio::io_context& io, const std::string& conn_string)
    : IDataBase{io, conn_string, [&] { Prepare(); }} {}

void DBNew::Prepare() {
  PrepareStmt("get_person_by_phone",
              "SELECT id, name, surname, middle_name FROM person WHERE phone_primary = $1;");
  PrepareStmt("get_person_importance",
              "SELECT t3.id, t3.priority, t3.title FROM account AS t1 "
              "INNER JOIN account_person AS t2 ON (t2.account_id = t1.id) "
              "LEFT OUTER JOIN account_importance AS t3 ON (t1.account_importance_id = t3.id) "
              "WHERE (t2.person_id = $1 AND t3.priority IS NOT NULL) ORDER BY t3.priority DESC;");
}
DBOld::DBOld(boost::asio::io_context& io, const std::string& conn_string)
    : IDataBase{io, conn_string, [&] { Prepare(); }} {}

void DBOld::Prepare() {}

}  // namespace alfa
