#ifndef ALFA_VOICED_H
#define ALFA_VOICED_H

#include <map>
#include <thread>
#include <vector>

#include <boost/asio.hpp>

#include "aricpp/client.h"
#include "fmt/format.h"
#include "utils/logger.h"
#include "utils/options.h"

#include "call.h"
#include "db.h"

namespace alfa {

struct AriInfo {
  AriInfo() = default;
  void FromOpts(const Options& opts) {
    host = opts.Get<std::string>("ari-host");
    port = opts.Get<std::string>("ari-port");
    user = opts.Get<std::string>("ari-user");
    pass = opts.Get<std::string>("ari-pass");
    apps = opts.Get<std::string>("ari-apps");
  }
  std::string Dump() const { return fmt::format("{}:{} ({};{})", host, port, user, pass); }

  std::string host;
  std::string port;
  std::string user;
  std::string pass;
  std::string apps;
};

class Voiced {
 public:
  Voiced(int argc, char** argv);
  ~Voiced();

  void Start();
  void Stop();
  void SetupEvents();

 private:
  boost::asio::io_context io_;
  boost::asio::signal_set signals_;

  std::vector<std::unique_ptr<std::thread>> io_threads_;
  aricpp::Client::Ptr client_;
  AriInfo ari_;

  std::map<std::string, Call::Ptr> calls_;

  DBPool<DBNew>::Ptr pool_new_;
  DBPool<DBOld>::Ptr pool_old_;
};

}  // namespace alfa

#endif  // ALFA_VOICED_H
