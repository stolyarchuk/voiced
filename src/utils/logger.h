#pragma once

#include <iostream>
#include <iterator>
#include <regex>
#include <string>

#include <boost/log/core/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/expressions/formatters/date_time.hpp>
#include <boost/log/expressions/formatters/named_scope.hpp>
#include <boost/log/sources/global_logger_storage.hpp>
#include <boost/log/sources/severity_feature.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/formatter_parser.hpp>

namespace logging = boost::log;

std::string ComputeMethodName(const std::string& pretty_function);

enum SeverityLevel { DEBUG = 0, INFO = 1, WARNING = 2, ERROR = 3, FATAL = 4, MAIN = 5 };

struct LoggerConfig {
  inline static std::vector<std::string> kStrings{"debug", "info", "warning", "error", "fatal"};
  inline static SeverityLevel kLevel{SeverityLevel::WARNING};
  void SetLevel(SeverityLevel level) { LoggerConfig::kLevel = level; }
};

using logger_t = boost::log::sources::severity_logger_mt<SeverityLevel>;

BOOST_LOG_GLOBAL_LOGGER(logger, logger_t)

#ifdef VOICED_DEBUG
#define LOG(severity) \
  BOOST_LOG_SEV(logger::get(), severity) << "[" << ComputeMethodName(__PRETTY_FUNCTION__) << ":" << __LINE__ << "] "
#else
#define LOG(severity) BOOST_LOG_SEV(logger::get(), severity)
#endif

#define SET_LOG_LEVEL(x)                                           \
  do {                                                             \
    LoggerConfig().SetLevel(x);                                    \
    LOG(MAIN) << "using log level: " << LoggerConfig::kStrings[x]; \
  } while (0)
